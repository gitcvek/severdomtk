<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
	<?php
		//Регистрируем файлы скриптов в <head>
		if(YII_DEBUG) {
			Yii::app ()->assetManager->publish (YII_PATH . '/web/js/source',false,-1,true);
		}

		Yii::app ()->clientScript->registerCoreScript ('jquery.project');
		Yii::app ()->clientScript->registerCoreScript ('bootstrap');
		$bootstrapFont = Yii::getPathOfAlias ('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
		Yii::app ()->clientScript->addDependResource ('bootstrap.min.css',array(
			$bootstrapFont . 'glyphicons-halflings-regular.eot' => '../fonts/',
			$bootstrapFont . 'glyphicons-halflings-regular.svg' => '../fonts/',
			$bootstrapFont . 'glyphicons-halflings-regular.ttf' => '../fonts/',
			$bootstrapFont . 'glyphicons-halflings-regular.woff' => '../fonts/',
		));

		Yii::app ()->clientScript->registerScriptFile ('/themes/business/js/js.js',CClientScript::POS_HEAD);

		Yii::app ()->clientScript->registerScript ('setScroll',"setAnchor();",CClientScript::POS_READY);
		Yii::app ()->clientScript->registerScript ('menu.init',"$('.dropdown-toggle').dropdown();",CClientScript::POS_READY);

		Yii::app ()->clientScript->registerCssFile ('/themes/business/css/content.css');
		Yii::app ()->clientScript->registerCssFile ('/themes/business/css/page.css');
		Yii::app ()->clientScript->registerCssFile ('/themes/business/css/jquery.fancybox.css');
	?>
	<title><?php echo CHtml::encode ($this->getPageTitle ()); ?></title>
</head>
<body>
<div class="mainContainer">
<div class="b-headContainer">
	<div class="logo">
		<img src="/themes/business/gfx/logo.jpg">
	</div>
	<div class="handler">
		<h1>ООО "Севердом ТК"</h1>

		<h2>Срубы из оцилиндрованных брёвен. Профилированный брус.</h2>

		<h2>Пиломатериалы. Оцилиндрованный обапол.</h2>
	</div>
</div>
<section class="choords">
	<div class="container">
		<div class="row">
			<div class="b-mapContainer color-bg">
				<div class="address">
					<p>
						Адрес: Республика Коми, г. Сыктывкар, м. Дырнос, 3/5<br>
						Тел./факс (8-8212) 32-14-28<br>
						Тел. (8-912) 86-59-530<br>
						e-mail:
						<a href="mailto:spetsles-komi@yandex.ru">spetsles-komi@yandex.ru</a>
					</p>
				</div>
				<div class="map-widget">
					<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=E4VXwU-9bPI_n1WZ5eH1pSAI3KCOQB0X&width=480&height=250"></script>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="b-mapContainer color-bg">
				<div class="address">
					<p>
						Адрес производственного подразделения:
						<br>
						Республика Коми, Усть-Вымский район, п. Студенец, м. Чернам, 16а<br>
						Тел./факс (8-82134) 22-484<br>
						Тел. (8-906) 88-02-199<br>
						e-mail:
						<a href="mailto:geha-72@mail.ru">geha-72@mail.ru</a>
					</p>
				</div>
				<div class="map-widget">
					<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=hvX8Fkb38nSFgbDHzLBo6WfrD8m7rk45&width=480&height=250"></script>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="b-mapContainer color-bg">
				<div class="address-no-map">
					<p>
						Представительство в г. Ульяновск.<br>
						432017, г. Ульяновск, ул. Пушкинская, д. 7.<br>
						Тел/факс (8-8422) 32-08-81<br>
						Тел. (8-917) 63-88-509<br>
						e-mail: <a href="mailto:seu.stepanov@yandex.ru">seu.stepanov@yandex.ru</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="about">
	<div class="container">
		<div class="row color-bg">
			<p>
				ООО «Севердом ТК» образовано в 2010 г.
				Основными видами деятельности компании являются производство срубов из оцилиндрованных брёвен, пиломатериалов, профилированного бруса и оцилиндрованного обапола.
				Производственное подразделение компании расположено в п. Студенец Усть-Вымского района Республики Коми.
				Продукция компании выпускается на трёх производственных линиях.
			</p>
		</div>
	</div>
</section>
<section class="first-line">
	<div class="container">
		<div class="row">
			<h2>Первая линия. Производство срубов.</h2>
		</div>
		<div class="row color-bg">
			<p>
				Линия организована на базе оцилиндровочного станка «Калибр-240» с роторно-вихревой головкой (г. Новосибирск). В состав линии входят также чашкорезный, торцовочный и фрезерный станки (г. Ижевск).
				Для изготовления срубов используется сосна из центральных районов Республики Коми. Диаметр брёвен срубов – до 24 см.
			</p>
		</div>
		<div class="row color-bg">
			<div class="b-block-elements">
				<h2>Элементы срубов</h2>
				<a class="gallery" title="Элементы срубов" rel="group" href="/themes/business/gfx/elements.png"><img src="/themes/business/gfx/elements.png"></a>
			</div>
			<div class="b-block-elements-sizes">
				<h2>Размеры элементов срубов</h2>
				<table class="info-table vertical-size">
					<th>Диаметр бревна, Д, мм</th>
					<th>Ширина укладочного паза, В, мм</th>
					<th>Высота бревна без паза, Н, мм</th>
					<tr>
						<td>200</td>
						<td>115</td>
						<td>160</td>
					</tr>
					<tr>
						<td>220</td>
						<td>125</td>
						<td>178</td>
					</tr>
					<tr>
						<td>240</td>
						<td>135</td>
						<td>198</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="b-first-line-img color-bg">
				<div class="b-first-line-img-left">
					<a class="gallery" title="Первая линия. Производство срубов." rel="group" href="/themes/business/gfx/first-line.jpg"><img src="/themes/business/gfx/first-line.jpg"></a>
				</div>
				<div class="b-first-line-img-right">
					<a class="gallery" title="Первая линия. Производство срубов." rel="group" href="/themes/business/gfx/first-line1.jpg"><img src="/themes/business/gfx/first-line1.jpg"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="second-line">
	<div class="container">
		<div class="row">
			<h2>Вторая линия. Производство досок, профилированного бруса и оцилиндрованного обапола.</h2>
		</div>
		<div class="row color-bg">
			<p>
				Линия организована на базе двух фрезерно-брусующих станков 668-С «Шервуд» (г. Киров).
				Линия используется для переработки хвойного тонкомерного пиловочника диаметром 12-20 см.
				Для изготовления профилированного бруса используется сосна из центральных районов Республики Коми. Сечение бруса – до 150*150 мм.
			</p>
		</div>
		<div class="row">
			<div class="b-second-line-img color-bg">
				<div class="b-second-line-img-left">
					<a class="gallery" title="Вторая линия. Производство досок, профилированного бруса и оцилиндрованного обапола." rel="group" href="/themes/business/gfx/second-line.jpg"><img src="/themes/business/gfx/second-line.jpg"></a>
				</div>
				<div class="b-second-line-img-right">
					<a class="gallery" title="Вторая линия. Производство досок, профилированного бруса и оцилиндрованного обапола." rel="group" href="/themes/business/gfx/second-line1.jpg"><img src="/themes/business/gfx/second-line1.jpg"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="third-line">
	<div class="container">
		<div class="row">
			<h2>Третья линия. Производство досок и бруса.</h2>
		</div>
		<div class="row color-bg">
			<p>
				Линия организована на базе двух ленточнопильных станков ПТ-03У «Крона», дискового многопильного и кромкообрезных станков.
				Линия используется для переработки хвойного пиловочника диаметром 20 см и выше.
			</p>

			<p>
				Готовая продукция складывается в транспортные пакеты, упаковывается лентой и грузится башенным краном в автомобильный транспорт.
			</p>

			<p>
				Покупателями продукции компании являются частные лица и предприятия, расположенные в Республике Коми и других регионах Европейской части Российской Федерации.
			</p>
		</div>
	</div>
</section>
<section class="projects">
	<div class="container">
		<div class="row">
			<h2>Некоторые реализованные проекты</h2>
		</div>
		<div class="row">
			<div class="b-hotel-img color-bg">
				<h2>Комплекс мини-гостиниц</h2>
				Заказчик Новиков А.П. т. 8 912 147 4105
				<br>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel0.jpg"><img src="/themes/business/gfx/hotel0.jpg"></a>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel1.jpg"><img src="/themes/business/gfx/hotel1.jpg"></a>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel2.jpg"><img src="/themes/business/gfx/hotel2.jpg"></a>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel3.jpg"><img src="/themes/business/gfx/hotel3.jpg"></a>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel4.jpg"><img src="/themes/business/gfx/hotel4.jpg"></a>
				<a class="gallery" title="Комплекс мини-гостиниц" rel="group" href="/themes/business/gfx/hotel5.jpg"><img src="/themes/business/gfx/hotel5.jpg"></a>
			</div>
		</div>
		<div class="row">
			<div class="b-house-img color-bg">
				<h2>Жилые дома</h2>

				<div class="b-house-img-left">Заказчик Галян Г.Г. тел: 8 (906) 880-21-99<br>
					<a class="gallery" title="Жилой дом" rel="group" href="/themes/business/gfx/house1.jpg"><img src="/themes/business/gfx/house1.jpg"></a>
				</div>
				<div class="b-house-img-right">Заказчик Лужинский А.А. тел: 8 (912)141-69-65<br>
					<a class="gallery" title="Жилой дом" rel="group" href="/themes/business/gfx/house2.jpg"><img src="/themes/business/gfx/house2.jpg"></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="b-banya-img color-bg">
				<h2>Бани</h2>

				<div class="b-banya-img-left">Заказчик Кучин В.А. тел: 8 (912) 868-77-09<br>
					<a class="gallery" title="Баня" rel="group" href="/themes/business/gfx/banya1.jpg"><img src="/themes/business/gfx/banya1.jpg"></a>
				</div>
				<div class="b-banya-img-right">Заказчик Васильев Д.А. тел: 8 (912)864-01-95<br>
					<a class="gallery" title="Баня" rel="group" href="/themes/business/gfx/banya2.jpg"><img src="/themes/business/gfx/banya2.jpg"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->widget ('application.modules.shop.widgets.catalog.CatalogWidget'); ?>
<section class="guest-info">
	<div class="container">
		<div class="row">
			<h2>Информация для посетителей сайта</h2>
			<?php echo $content; ?>
		</div>
	</div>
</section>
<footer>
	<div class="footer-container">
		<div class="row">
			<div class="b-contacts-company-name">
				OOO "Севердом ТК"
			</div>
			<div class="metrika">
			<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=26743446&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/26743446/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:26743446,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->
			</div>
			<div class="cvek-logo" onclick="window.open('http://cvek.ru')"></div>
		</div>
	</div>
</footer>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter26743446 = new Ya.Metrika({id:26743446,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/26743446" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->



<script type="text/javascript" src="/themes/business/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/themes/business/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/themes/business/js/jquery.fancybox-1.2.1.pack.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("a.gallery, a.iframe").fancybox();
			$("a.modalbox").attr("href", url);	
			$("a.modalbox").fancybox(
			{								  
			"frameWidth" : 600,	 
			"frameHeight" : 400 								  
			});
			});
	</script>
</body>
</html>