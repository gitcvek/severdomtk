<?php

	class CatalogWidget extends DaWidget{
		public function run (){
			$products = Product::model ()->findAll ();
			if (!$products) return;
			$this->render ('catalog',array(
				'products' => $products,
			));
		}
	}
