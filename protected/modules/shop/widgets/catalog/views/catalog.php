<section class="catalog">
	<div class="container">
		<div class="row">
			<h2>Каталог продукции для свободной реализации<br> по состоянию на <?php echo Yii::app()->dateFormatter->format('d MMMM y',$products[0]->category->date); ?>
			</h2>
		</div>
		<div class="row">
			<table class="info-table full-table">
				<th>№ П/П</th>
				<th>Наименование</th>
				<th>Количество</th>
				<th>Цена</th>
				<?php foreach( $products as $key => $product ): ?>
					<tr>
						<td><?php echo ++$key; ?></td>
						<td><?php echo $product->name; ?></td>
						<td><?php echo $product->remain; ?></td>
						<td><?php echo $product->retail_price; ?> руб.</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
</section>